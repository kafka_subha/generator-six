package com.subha.kafka.route;

import com.subha.kafka.processor.GeneratorSixProcessor;
import org.apache.camel.*;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaComponent;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.component.kafka.embedded.EmbeddedKafkaBroker;
import org.apache.camel.component.kafka.embedded.EmbeddedZookeeper;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.impl.JndiRegistry;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.AvailablePortFinder;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.*;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created by if993886 on 08/07/2018.
 */
public class GeneratorSixRouteTest extends CamelTestSupport {



    GeneratorSixProcessor generatorSixProcessor = new GeneratorSixProcessor();

    @ClassRule
    public static EmbeddedZookeeper zookeeper = new EmbeddedZookeeper(
            AvailablePortFinder.getNextAvailable(23000));

    @ClassRule
    public static EmbeddedKafkaBroker kafkaBroker =
            new EmbeddedKafkaBroker(0,
                    AvailablePortFinder.getNextAvailable(24000),
                    zookeeper.getConnection(),
                    new Properties());

    private static final Logger LOG = LoggerFactory.getLogger(GeneratorSixRouteTest.class);

    @Produce(uri = "direct:start")
    private ProducerTemplate stringsTemplate;

    private static final String TOPIC_STRINGS_IN_HEADER = "testHeader";

    private static final String FROM_TOPIC = "seed-topic";
    private static final String TO_TOPIC = "gen-topic";

    @EndpointInject(uri = "kafka:" + FROM_TOPIC
            + "?autoOffsetReset=earliest"
            + "&groupId=TEST&consumersCount=1"
    )
    private Endpoint from;

    @EndpointInject(uri = "kafka:" + TO_TOPIC + "?requestRequiredAcks=-1")
    private Endpoint to;

    private org.apache.kafka.clients.producer.KafkaProducer<String, String> producer;
    private org.apache.kafka.clients.consumer.KafkaConsumer<String, String> consumer;


    private static int getZookeeperPort() {
        return zookeeper.getPort();
    }

    private static int getKafkaPort() {
        return kafkaBroker.getPort();
    }



    @BeforeClass
    public static void beforeClass() {
        LOG.info("### Embedded Zookeeper connection: " + zookeeper.getConnection());
        LOG.info("### Embedded Kafka cluster broker list: " + kafkaBroker.getBrokerList());
    }

    @Override
    protected Properties useOverridePropertiesWithPropertiesComponent() {
        Properties prop = new Properties();

        prop.setProperty("message.interval", "100");
        prop.setProperty("kafka.from-topic", "seed-topic");
        prop.setProperty("kafka.to-topic", "gen-topic");
        prop.setProperty("kafka.server", "localhost");
        prop.setProperty("kafka.port", "24000");
        prop.setProperty("kafka.channel", "TEST");

        return prop;
    }

    private Properties getDefaultProducerProperties() {
        Properties props = new Properties();
        LOG.info("Connecting to Kafka port {}", kafkaBroker.getPort());
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBroker.getBrokerList());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, KafkaConstants.KAFKA_DEFAULT_SERIALIZER);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaConstants.KAFKA_DEFAULT_SERIALIZER);
        props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, KafkaConstants.KAFKA_DEFAULT_PARTITIONER);
        props.put(ProducerConfig.ACKS_CONFIG, "1");
        return props;
    }

    private Properties getDefaultSenderProperties() {
        Properties stringsProps = new Properties();

        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:" + getKafkaPort());
        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG, "DemoConsumer");
        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true");
        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000");
        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");
        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        stringsProps.put(org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return stringsProps;

    }

    @Override
    protected JndiRegistry createRegistry() throws Exception {
        JndiRegistry jndi = super.createRegistry();

        Properties prop = new Properties();
        prop.setProperty("zookeeperPort", "" + getZookeeperPort());
        prop.setProperty("kafkaPort", "" + getKafkaPort());
        MockitoAnnotations.initMocks(this);
        jndi.bind("genSix", generatorSixProcessor);
        jndi.bind("prop", prop);
        return jndi;
    }

    @Override
    protected CamelContext createCamelContext() throws Exception {
        CamelContext context = super.createCamelContext();
        context.addComponent("properties", new PropertiesComponent("ref:prop"));

        KafkaComponent kafka = new KafkaComponent(context);
        kafka.setBrokers("localhost:" + getKafkaPort());
        context.addComponent("kafka", kafka);

        return context;
    }


    @Before
    public void before() throws Exception {
        Properties props = getDefaultProducerProperties();
        producer = new org.apache.kafka.clients.producer.KafkaProducer<>(props);
        consumer = new KafkaConsumer<>(getDefaultSenderProperties());

    }


    @After
    public void after() {
        if (producer != null) {
            producer.close();
        }
        if (consumer != null) {
            consumer.close();
        }
    }


    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new GeneratorSixRoute();
    }

    @Test
    public void checkGeneratorSixConsumer() throws Exception {

        RouteDefinition routeDefinition = context.getRouteDefinition("generator-six").adviceWith(context, new AdviceWithRouteBuilder() {

            @Override
            public void configure() throws Exception {
                replaceFromWith("direct:start");
                interceptSendToEndpoint("kafka:gen-topic?brokers=localhost:24000&groupId=TEST&autoOffsetReset=earliest&consumersCount=1").skipSendToOriginalEndpoint()
                        .to(to);
            }
        });

        int messageInTopic = 1;

        CountDownLatch messagesLatch = new CountDownLatch(messageInTopic);
        context.startRoute("generator-six");

        sendMessagesInRoute(messageInTopic, stringsTemplate, "2-GAME1", KafkaConstants.PARTITION_KEY, "1");

        ConsumerRecords<String, String> records = createKafkaMessageConsumer(consumer, TO_TOPIC, TOPIC_STRINGS_IN_HEADER, messagesLatch);

        boolean allMessagesReceived = messagesLatch.await(200, TimeUnit.MILLISECONDS);

        assertTrue("Not all messages were published to the kafka topics. Not received: " + messagesLatch.getCount(), allMessagesReceived);
        records.forEach(stringStringConsumerRecord -> {
            assertEquals("Expected is not matching with actual ",stringStringConsumerRecord.value(), "8-GEN6-GAME1");
        });

        context.stop();
    }

    @Test
    public void checkGeneratorSixProducer() throws Exception {

        RouteDefinition routeDefinition = context.getRouteDefinition("generator-six").adviceWith(context, new AdviceWithRouteBuilder() {

            @Override
            public void configure() throws Exception {
                replaceFromWith(from);
                interceptSendToEndpoint("kafka:gen-topic?brokers=localhost:24000").skipSendToOriginalEndpoint()
                        .to("mock:result");
            }
        });

        MockEndpoint mockTo = getMockEndpoint("mock:result");
        mockTo.expectedBodiesReceivedInAnyOrder("64-GEN6-GAME1");
        context.startRoute("generator-six");
        ProducerRecord<String, String> data = new ProducerRecord<>(FROM_TOPIC, "1", "4-GAME1");
        producer.send(data);
        mockTo.assertIsSatisfied();

        context.stop();
    }


    private ConsumerRecords<String, String> createKafkaMessageConsumer(KafkaConsumer<String, String> consumerConn,
                                                                       String topic, String topicInHeader, CountDownLatch messagesLatch) {

        consumerConn.subscribe(Arrays.asList(topic, topicInHeader));
        boolean run = true;
        ConsumerRecords<String, String> returnRecords = null;
        while (run) {
            ConsumerRecords<String, String> records = consumerConn.poll(100);
            for (int i = 0; i < records.count(); i++) {
                messagesLatch.countDown();
                if (messagesLatch.getCount() == 0) {
                    run = false;
                    returnRecords = records;
                }
            }
        }
        return returnRecords;
    }

    private void sendMessagesInRoute(int messages, ProducerTemplate template, Object bodyOther, String... headersWithValue) {
        Map<String, Object> headerMap = new HashMap<>();
        if (headersWithValue != null) {
            for (int i = 0; i < headersWithValue.length; i = i + 2) {
                headerMap.put(headersWithValue[i], headersWithValue[i + 1]);
            }
        }
        sendMessagesInRoute(messages, template, bodyOther, headerMap);
    }

    private void sendMessagesInRoute(int messages, ProducerTemplate template, Object bodyOther, Map<String, Object> headerMap) {
        for (int k = 0; k < messages; k++) {
            template.sendBodyAndHeaders(bodyOther, headerMap);
        }
    }
}
