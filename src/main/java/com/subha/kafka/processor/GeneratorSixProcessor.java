package com.subha.kafka.processor;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 03/07/2018.
 */

@Component("genSix")
public class GeneratorSixProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(GeneratorSixProcessor.class);

    public void process(Exchange exchange) {
        int seed = Integer.parseInt(exchange.getIn().getBody().toString().split("-")[0]);
        long generatedResult = seed*seed*seed;

        LOG.info("Got seed " + seed);
        LOG.info("Generator one generated number " + generatedResult);

        exchange.getIn().setBody(generatedResult + "-GEN6-" + exchange.getIn().getBody().toString().split("-")[1]);
    }

}
